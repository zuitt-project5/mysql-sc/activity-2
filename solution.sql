CREATE DATABASE blog_db;

USE blog_db;

CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	email VARCHAR(100) NOT NULL,
	password VARCHAR(50) NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE posts (
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	title VARCHAR(200) NOT NULL,
	content VARCHAR(5000) NOT NULL,
	datetime_posted DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE post_likes (
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	post_id INT NOT NULL,
	datetime_liked DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_post_likes_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_post_likes_post_id
		FOREIGN KEY (post_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT	
);

CREATE TABLE post_comments (
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	post_id INT NOT NULL,
	content VARCHAR(5000) NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_post_comments_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_post_comments_post_id
		FOREIGN KEY (post_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT		
);


-- -- Create a database
-- CREATE DATABASE blog_db;

-- --Create the students table
-- CREATE TABLE students (
-- 	id INT NOT NULL AUTO_INCREMENT,
-- 	student_name VARCHAR(50) NOT NULL,
-- 	PRIMARY KEY(id)
-- );

-- --Create the teachers table
-- CREATE TABLE teachers (
-- 	id INT NOT NULL AUTO_INCREMENT,
-- 	teacher_name VARCHAR(50) NOT NULL,
-- 	PRIMARY KEY(id)
-- );

-- --Create the courses table, which references our teachers table
-- CREATE TABLE courses (
-- 	id INT NOT NULL AUTO_INCREMENT,
-- 	course_name VARCHAR(50) NOT NULL,
-- 	teacher_id INT NOT NULL,
-- 	PRIMARY KEY(id),
-- 	CONSTRAINT fk_courses_teacher_id
-- 		FOREIGN KEY (teacher_id) REFERENCES teachers(id)
-- 		ON UPDATE CASCADE
-- 		ON DELETE RESTRICT
-- );

-- --Create the student_courses linking table, which links our students table with our courses table
-- CREATE TABLE student_courses (
-- 	id INT NOT NULL AUTO_INCREMENT,
-- 	student_id INT NOT NULL,
-- 	course_id INT NOT NULL,
-- 	PRIMARY KEY(id),
-- 	CONSTRAINT fk_student_courses_student_id
-- 		FOREIGN KEY (student_id) REFERENCES students(id)
-- 		ON UPDATE CASCADE
-- 		ON DELETE RESTRICT,
-- 	CONSTRAINT  fk_student_courses_course_id
-- 		FOREIGN KEY (course_id) REFERENCES courses(id)
-- 		ON UPDATE CASCADE
-- 		ON DELETE RESTRICT
-- );